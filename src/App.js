import {Route, Routes} from "react-router-dom";
import Layout from './layout/Layout'
import NewTemplate from './pages/NewTemplate'
import MainPage from "./pages/MainPage";
import Meetings from "./pages/Meetings";
import Template from './components/templates/Template'
import SignUpForMeeting from "./pages/SignUpForMeeting";
import EditTemplatePage from './pages/EditTemplatePage';
import {ReactKeycloakProvider} from "@react-keycloak/web";
import keycloak from "./components/authorization/Keycloak";
import HomePage from "./pages/HomePage";
import PrivateRoute from "./components/utils/PrivateRoute";

function App() {
  return (
    <ReactKeycloakProvider authClient={keycloak}>
      <Layout>
        <Routes>
          <Route path="/" element={<HomePage/>}/>
          <Route path="/all-templates" exact element={
            <PrivateRoute>
              <MainPage/>
            </PrivateRoute>}/>
          <Route path="/new-template" exact element={
            <PrivateRoute>
              <NewTemplate/>
            </PrivateRoute>}/>
          <Route path="/meetings" exact element={
            <PrivateRoute>
              <Meetings/>
            </PrivateRoute>}/>
          <Route path="/template/:templateId" exact element={
            <PrivateRoute>
              <Template/>
            </PrivateRoute>}/>
          <Route path="/template-edit/:templateId" exact element={
            <PrivateRoute>
              <EditTemplatePage/>
            </PrivateRoute>}/>
          <Route path="/sign-up/:templateId" exact element={
            <PrivateRoute>
              <SignUpForMeeting/>
            </PrivateRoute>}/>
        </Routes>
      </Layout>
    </ReactKeycloakProvider>
  );
}

export default App;
