import {Modal} from 'antd'
import 'antd/dist/antd.css';


export const successModal = ({title}) => {
  Modal.success({
    title
  });
};

export const errorModal = ({error,title}) => {
  Modal.error({
    title,
    content: `Код ошибки: ${error.response.status}`,
  });
};