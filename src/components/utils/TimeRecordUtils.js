import 'antd/dist/antd.css';
import * as _ from 'lodash';
import {DAYS_OF_WEEK} from '../../constants';
import moment from "moment";
import React from "react";

export const getDayMapping = (days, translate) => {
  return _.map(days, day => translate[day])
}

const mappedTimeIntervals = (timeIntervalsFromTemplate) => {
  if (!timeIntervalsFromTemplate) {
    return []
  }
  return _.filter(timeIntervalsFromTemplate, interval => !_.isEmpty(interval))
}

export const getDaysAndIntervals = ({timeIntervalsFromTemplate, daysFromTemplate}) => {
  const timesIntervals = mappedTimeIntervals(timeIntervalsFromTemplate)

  return _.reduce(DAYS_OF_WEEK, (acc, day) => {
    acc[day] = _.includes(daysFromTemplate, day)
      ? timesIntervals
      : [{}]
    return acc
  }, {})
}


const formatInterval = (date) => {
  return moment(date).hours() * 60 + moment(date).minutes()
}

const getTimeInterval = (intervals) => {
  const minuteDurations = _.map(intervals, interval => formatInterval(interval))
  const duration = moment.duration({
    minutes: minuteDurations[1] - minuteDurations[0]
  }).toISOString()

  const startTime = moment(intervals[0]).format("THH:mmZ")

  return `${startTime}/${duration}`
}

export const formatTemplate = (daysIntervals) => {
  if (daysIntervals === undefined) {
    return []
  }
  const parsedInterval = JSON.parse(daysIntervals)
  const days = _.keys(parsedInterval)

  return _.flatten(_.reduce(days, (acc, day) => {
    const weekday = day
    const dayIntervals = parsedInterval[day]

    if (!(_.isEmpty(dayIntervals) || _.isEmpty(dayIntervals[0]))) {
      const flattenedIntervals = _.map(dayIntervals, intervals => {
        const timeInterval = getTimeInterval(intervals)
        return {
          weekday,
          timeInterval,
        }
      })
      acc.push(flattenedIntervals)
    }
    return acc
  }, []))
}

export const getTimeIntervals = (timeIntervals) => {
  return _.map(timeIntervals, (elem, index) => {
    const split = elem.split('/')
    const from = moment(split[0], "HH:mm:ss").toString()
    const duration = moment.duration(split.slice(1)).asMinutes()
    const to = moment(new Date(from)).add(duration, 'minutes')
    return (
      <div key={index}>
        {`${moment(new Date(from)).format('HH:mm')} - ${to.format('HH:mm')}`}
      </div>
    )
  })
}
