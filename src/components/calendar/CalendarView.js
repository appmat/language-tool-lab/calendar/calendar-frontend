import Weekly from "./Weekly";
import classes from './CalendarView.module.css';
import moment from "moment";
import {useState} from "react";
import {Button, Tooltip} from 'antd'
import { RightOutlined, LeftOutlined } from '@ant-design/icons';
import 'antd/dist/antd.css'


function CalendarView({template}) {
  const start = moment().startOf('isoWeek').format('YYYY-MM-DD')
  const [startDate, setStartDate] = useState(start)

  const getWeek = (e) => {
    if (e === 'next'){
      const newStartDate = moment(startDate).add(7, 'd').format('YYYY-MM-DD')
      setStartDate(newStartDate)
    }
    else{
      const newStartDate = moment(startDate).subtract(7, 'd').format('YYYY-MM-DD')
      setStartDate(newStartDate)
    }
  }

  return (
    <div className={classes.calendar}>
      <div className={classes.btn}>
        <Tooltip title="Предыдущая неделя" className={classes.icon}>
          <Button type="dashed" onClick={() => getWeek('previous')} icon={<LeftOutlined />} />
        </Tooltip>
        <Tooltip title="Следующая неделя">
          <Button type="dashed" onClick={() => getWeek('next')} icon={<RightOutlined />} />
        </Tooltip>
      </div>
      <div className={classes.week}>
        <Weekly start={startDate} template={template}/>
      </div>
    </div>
  )
}

export default CalendarView;