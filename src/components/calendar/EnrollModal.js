import { Modal, Typography} from 'antd'
import React, {useState} from "react";
import {fromIndexToTime, numberToDay} from "./calendarUtils";
import { useParams} from "react-router-dom";
import axios from "axios";
import {baseUrl} from "../../constants";
import {errorModal, successModal} from "../utils/Modals";
import {useKeycloak} from "@react-keycloak/web";
import {UserOutlined, ClockCircleOutlined} from "@ant-design/icons";
import moment from "moment";
const { Title } = Typography;

const mappedDay = {
  'ПН': 'понедельник',
  'ВТ': 'вторник',
  'СР': 'среда',
  'ЧТ': 'четверг',
  'ПТ': 'пятница',
  'СБ': 'суббота',
  'ВС': 'воскресенье'
}
export default function EnrollModal({day, rowNumber, columnNumber, duration, week}) {
  const [isModalVisible, setIsModalVisible] = useState(true);
  let params = useParams()
  const { keycloak } = useKeycloak();
  const durationFormat = moment.duration(duration).asMinutes()
  const hours = moment(fromIndexToTime(rowNumber - 1), 'HH:mm').format('HH')
  const minutes = moment(fromIndexToTime(rowNumber - 1), 'HH:mm').format('mm')
  const date  = moment(week[columnNumber].date, 'DD.MM.YYYY').add(hours, 'hour').add(minutes, 'minutes')
  console.log('her', )

  const handleOk = async () => {
    const signUpInfo = {
      id: -1,
      meetingTemplateId: params.templateId,
      dateTimeInterval: `${date.format('YYYY-MM-DDTHH:mmZ')}/${duration.toString()}`,
      participantId: 1
    }
    let axiosConfig = {
      headers: {
        'Authorization': `Bearer ${keycloak.token}`
      }
    }
    try {
      await axios.post(`${baseUrl}/schedule`, signUpInfo, axiosConfig).then(() => {
        successModal({title: 'Запись успешно создана'})
      });
    } catch (error) {
      errorModal({error})
    }
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  return (
    <div>
      <Modal title="Запись на встречу" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
        <Title level={5}>{week[columnNumber].date}, {mappedDay[numberToDay[columnNumber]]}</Title>
        <Title level={5}><UserOutlined /> {day ? 'Занято' : 'Свободно'}</Title>
        <Title level={5}><ClockCircleOutlined /> {fromIndexToTime(rowNumber - 1)}, {durationFormat} минут</Title>
      </Modal>
    </div>
  )
}