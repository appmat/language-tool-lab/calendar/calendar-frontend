import classes from './style.module.css'
export default function CalendarHeader(week) {
  return (
    <div className={`${classes.row} ${classes.date}`}>
      {week.week.map((day, index) => (
          <div key={index} className={classes.block}>
            {index !== 0 ? day.date : ''}
            <br/>
            {index !== 0 ? day.weekDay : ''}
          </div>
        )
      )}
    </div>
  )
}