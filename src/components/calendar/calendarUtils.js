import moment from "moment";
import * as _ from "lodash";

export const numberToDay = {
  1: 'ПН',
  2: 'ВТ',
  3: 'СР',
  4: 'ЧТ',
  5: 'ПТ',
  6: 'СБ',
  7: 'ВС'
}

const getObjectOfDay = (startDate) => {
  return {
    date: startDate.add(1, 'day').format('DD.MM.YYYY'),
    weekDay: numberToDay[startDate.isoWeekday()]
  }
}

const getWeekDays = (start) => {
  let startDate = moment(start).isoWeekday(1).subtract(1, 'day')
  const emptyWeek = new Array(8).fill(null)
  return emptyWeek.map((elem, index) =>
    index === 0
      ? null
      : getObjectOfDay(startDate))
}

export const generateWeeklyMatrix = ({start = moment(), mapppedDates}) => {
  let emptyWeeklyMatrix = new Array(97)
  emptyWeeklyMatrix[0] = getWeekDays(start)

  for (let i = 1; i < emptyWeeklyMatrix.length; i++) {
    emptyWeeklyMatrix[i] = new Array(8).fill(true);
    emptyWeeklyMatrix[i][0] = i
  }

  for (let i = 0; i < _.size(mapppedDates); i++) {
    let curDate = mapppedDates[i]
    let column = curDate.isoWeekday()
    let row = Number(curDate.format('HH')) * 4 + Number(curDate.format('mm'))/60*4 + 1
    emptyWeeklyMatrix[row][column] = false
  }
  return emptyWeeklyMatrix
}

const addZeroToTime = ({time, period}) => {
  if (period === 'hour' && time.length === 1) {
    return '0' + time
  }
  if (period === 'minutes' && time.length === 1) {
    return time + '0'
  }
  return time
}

export const fromIndexToTime = (index) => {
  const hours = addZeroToTime({time: `${Math.floor(index/4)}`, period: 'hour'})
  const minutes = addZeroToTime({time: `${(index % 4)*15}`, period: 'minutes'})

  return `${hours}:${minutes}`
}
