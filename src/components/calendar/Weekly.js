import React, {useEffect, useState} from "react";
import {generateWeeklyMatrix} from './calendarUtils'
import {Slot} from './Slot'
import classes from './style.module.css'
import CalendarHeader from "./CalendarHeader";
import axios from "axios";
import * as _ from 'lodash';
import {baseUrl} from "../../constants";
import {useKeycloak} from "@react-keycloak/web";
import moment from "moment";
import {useParams} from "react-router-dom";

function Weekly({start}) {
  const { keycloak } = useKeycloak();
  let params = useParams()
  const [schedule, setSchedule] = useState({})
  const [duration, setDuration] = useState('')

  useEffect(() => {
    if (!!keycloak.authenticated) {
      axios.get(
        `${baseUrl}/schedule/${params.templateId}?start=${start}&zoneOffset=%2B03:00&days=7`,
        {
          headers: {
            'Authorization': `Bearer ${keycloak.token}`
          }
        })
        .then(response => {
          setSchedule(response.data)
          setDuration(_.get(_.head(_.get(response.data, 'slots', [])), 'dateTimeInterval').split('/')[1])
        })
    }
  }, [params.templateId, keycloak, start])
  const weekCalendar = generateWeeklyMatrix({start, mapppedDates: _.map(_.get(schedule, 'slots'), 'dateTimeInterval').map(elem => moment(elem.split('/')[0]))})

  return (
    <div>
      <CalendarHeader week={weekCalendar[0]}/>
      {weekCalendar.map((row, rowNumber) => (
        <React.Fragment key={rowNumber}>
          <div className={classes.row}>
            {rowNumber !== 0 && (
              row.map((day, columnNumber) => (
                  <Slot day={day} key={columnNumber} rowNumber={rowNumber} columnNumber={columnNumber} duration={duration} week={weekCalendar[0]}/>
            )
            ))}
          </div>
        </React.Fragment>
      ))}
    </div>
  )
}

export default Weekly;