import classes from './style.module.css'
import {fromIndexToTime,} from "./calendarUtils";
import EnrollModal from "./EnrollModal";
import {useState} from "react";


const addStyleToBusyBlock = (day) => {
  if (day) {
    return classes.busy
  }
}

export function Slot({day, rowNumber, columnNumber, duration, week}) {
  const [isModalVisible, setIsModalVisible] = useState(false);

  return (
    (columnNumber === 0) && (rowNumber !== 0)
      ? (
        <div className={`${classes.block} ${classes.time}`}>
          {fromIndexToTime(day - 1)}
        </div>
      )
      : (
        <div
          className={`${classes.block} ${addStyleToBusyBlock(day)}`}
          onClick={() => setIsModalVisible(true)}
        >
          {day}
          {isModalVisible && <EnrollModal day={day} rowNumber={rowNumber} columnNumber={columnNumber}  duration={duration} week={week}/>}
        </div>
      )
  )
}
