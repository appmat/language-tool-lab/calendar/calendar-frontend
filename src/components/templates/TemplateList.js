import classes from "./Templates.module.css";
import TemplateItem from "./TemplateItem";
import {Link} from "react-router-dom";
import {Card} from "antd";

function TemplateList(props) {
  return (
    <div className={classes.content}>
      {props.templates.map((template, index) => (
        <div className={classes.card} key={index}>
          <TemplateItem
            id={template.id}
            title={template.title}
            durationMin={template.durationMin}
            description={template.description}
            place={template.place}
          />
        </div>
      ))}
      <div className={classes.card}>
        <Card size="small" title="" style={{width: 300}}>
          <p><Link to="/new-template">
            Добавить новый шаблон
          </Link></p>
        </Card>
      </div>
    </div>
  )
}

export default TemplateList;