import { useParams } from "react-router-dom";
import {useEffect, useState} from "react";
import axios from "axios";
import {baseUrl} from "../../constants";
import TemplateViewing from "../newTemplate/TemplateViewing";
import {useKeycloak} from "@react-keycloak/web";

function Template () {
  let params = useParams();
  const [template, setTemplate] = useState({})
  const { keycloak } = useKeycloak();
  console.log(keycloak.token)
  useEffect(() => {
    axios.get(`${baseUrl}/meeting-template/${params.templateId}`,{
      headers: {
        'Authorization': `Bearer ${keycloak.token}`
      }
    })
      .then(response => setTemplate(response.data))
  }, [params.templateId, keycloak])

  return (
    <div>
      <TemplateViewing template={template}/>
    </div>
  )
}

export default Template;