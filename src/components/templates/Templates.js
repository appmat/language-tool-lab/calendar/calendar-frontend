import classes from './Templates.module.css'
import TemplateList from './TemplateList'
import {useEffect, useState} from "react";
import axios from "axios";
import * as _ from 'lodash'
import {baseUrl} from "../../constants";
import {useKeycloak} from "@react-keycloak/web";


function Templates() {
  const [templates, setTemplates] = useState([])
  const [offset, setOffset] = useState(0)
  const [fetching, setFetching] = useState(true)
  const [allData, setAllData] = useState(false)
  const limit = 20
  const { keycloak } = useKeycloak();

  useEffect(() => {
    if (fetching && !allData) {
      axios.get(`${baseUrl}/meeting-template?offset=${offset}&limit=${limit}`, {
        headers: {
          'Authorization': `Bearer ${keycloak.token}`
        }
      })
        .then(response => {
          setTemplates(prevState => [...prevState, ...response.data.list])
          setOffset(prevState => prevState + limit)
          setAllData(_.isEmpty(response.data.list))
        })
        .finally(() => setFetching(false))
    }
  }, [fetching, allData, offset, limit, keycloak])

  useEffect(() => {
    document.addEventListener('scroll', scrollHandler)
    return function () {
      document.removeEventListener('scroll', scrollHandler)
    }
  }, [])

  const scrollHandler = (e) => {
    if (e.target.documentElement.scrollHeight - (e.target.documentElement.scrollTop + window.innerHeight) < 100){
      setFetching(true)
    }
  }

  return (
    <div className={classes.layout}>
      <h1>Шаблоны для записи</h1>
      <TemplateList templates={templates}/>
    </div>
  )
}

export default Templates;
