import {Card} from "antd";
import {Link} from "react-router-dom";
import {Button, Tooltip} from "antd";
import {CopyToClipboard} from 'react-copy-to-clipboard';
import {UnorderedListOutlined} from '@ant-design/icons';
import {serviceUrl} from '../../constants'

function TemplateItem(props) {
  return (
    <Card size="small" title={props.title}
          extra={
            <Link to={'/template/' + props.id}>
              <UnorderedListOutlined key="ellipsis"/>
            </Link>
          }
          style={{width: 300}}
    >
      <p>{props.durationMin} минут</p>
      <p>{props.description}</p>
      <p>{props.place}</p>
      <p>
        <CopyToClipboard text={`${serviceUrl}/sign-up/${props.id}`}>
          <Tooltip title="Скопировать" color={"#CECEDAFF"}>
            <Button type="link">Ссылка на запись</Button>
          </Tooltip>
        </CopyToClipboard>
      </p>
    </Card>
  )
}

export default TemplateItem;