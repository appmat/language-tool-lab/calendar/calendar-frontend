import classes from './TimeRecord.module.css'
import mainClasses from './NewTemplateForm.module.css'
import 'antd/dist/antd.css';
import React, {useState} from 'react'
import * as _ from 'lodash';
import moment from "moment";
import {Button, Checkbox, TimePicker} from "antd";
import {CloseOutlined} from "@ant-design/icons";
import {dayMapping, dayMappingToEng, DAYS_OF_WEEK, DEFAULT_DAY_OF_WEEK} from "../../constants";
import {getDaysAndIntervals, getDayMapping} from "../utils/TimeRecordUtils";

const defaultIntervals = {
  MONDAY: [],
  TUESDAY: [],
  WEDNESDAY: [],
  THURSDAY: [],
  FRIDAY: [],
  SATURDAY: [],
  SUNDAY: []
}

function TimesPreview({parenCallback}) {
  const [daysAndIntervals, setDaysAndIntervals] = useState(defaultIntervals)
  const [showTimeTemplate, setShowTimeTemplate] = useState(false)

  const [timeIntervals, setTimeIntervals] = useState([{}])
  const [days, setDays] = useState(getDayMapping(DEFAULT_DAY_OF_WEEK, dayMapping))

  let handleChangeIntervals = (index, time) => {
    let newTimeIntervals = [...timeIntervals];
    newTimeIntervals[index] = time;
    setTimeIntervals(newTimeIntervals);
  }
  const addInterval = () => {
    setTimeIntervals([...timeIntervals, {}])
  }
  const removeInterval = (i) => {
    let newTimeIntervals = [...timeIntervals];
    newTimeIntervals.splice(i, 1);
    setTimeIntervals(newTimeIntervals)
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    const res = getDaysAndIntervals({
      timeIntervalsFromTemplate: timeIntervals,
      daysFromTemplate: getDayMapping(days, dayMappingToEng)
    })
    setDaysAndIntervals(res)
    parenCallback(JSON.stringify(res))
  }

  function handleChangeDays(checkedValues) {
    setDays(checkedValues)
  }

  let addIntervalToDay = (day) => {
    const newDayIntervals = [...daysAndIntervals[day], {}]
    setDaysAndIntervals({
      ...daysAndIntervals,
      [day]: newDayIntervals
    })
    parenCallback(JSON.stringify(daysAndIntervals))
  }
  let removeIntervalFromDay = (index, day) => {
    let newDayIntervals = daysAndIntervals[day]
    newDayIntervals.splice(index, 1)
    setDaysAndIntervals({
      ...daysAndIntervals,
      [day]: newDayIntervals
    })
    parenCallback(JSON.stringify(daysAndIntervals))
  }
  const handleChangeToDays = (index, time, day) => {
    let previousIntervals = daysAndIntervals[day]
    previousIntervals[index] = time
    setDaysAndIntervals({
      ...daysAndIntervals,
      [day]: previousIntervals
    })
    parenCallback(JSON.stringify(daysAndIntervals))
  }

  return (
    <div>
      Добавьте доступные интервалы для записи
      <div className={classes.block}>
        <Button onClick={() => setShowTimeTemplate(!showTimeTemplate)}>
          Создать шаблон времени
        </Button>
        {showTimeTemplate
          ? <div className={mainClasses.time}>
          <div className={classes.block}>
            <div className={classes.timeblock}>
              {timeIntervals.map((element, index) => (
                <div className={classes.interval} key={index}>
                  <TimePicker.RangePicker
                    className={classes.time}
                    format="HH:mm"
                    minuteStep={15}
                    onChange={e => handleChangeIntervals(index, e)}
                    value={_.isEmpty(element) ? {} : element}
                    placeholder={['start time', 'end time']}
                  />
                  {
                    index ?
                      <Button classes={classes.remove} icon={<CloseOutlined/>} onClick={() => removeInterval(index)}/>
                      : null
                  }
                </div>
              ))}
              <div className={classes.btn}>
                <Button onClick={() => addInterval()}>Добавить время</Button>
              </div>
            </div>
            <div className={classes.item}>Применить шаблон к дням</div>
            <Checkbox.Group
              className={classes.item}
              options={getDayMapping(DAYS_OF_WEEK, dayMapping)}
              defaultValue={getDayMapping(DEFAULT_DAY_OF_WEEK, dayMapping)}
              onChange={handleChangeDays}
              value={days}
            />
            <div>
              <Button onClick={handleSubmit}>Применить</Button>
            </div>
          </div>
        </div>
          : ""}
        <div>
          {_.keys(daysAndIntervals).map((day, numberOfDay) => (
            <div className={classes.interval} key={numberOfDay}>
              {dayMapping[day]}
              {daysAndIntervals[day].map((interval, index) => (
                <div key={index}>
                  <TimePicker.RangePicker
                    format="HH:mm"
                    minuteStep={15}
                    onChange={e => handleChangeToDays(index, e, day)}
                    value={_.isEmpty(interval) ? {} : [moment(interval[0]), moment(interval[1])]}
                    placeholder={['start time', 'end time']}
                  />
                  <Button classes={classes.remove} icon={<CloseOutlined/>}
                          onClick={() => removeIntervalFromDay(index, day)}/>
                </div>
              ))}
              <div className={classes.btn}>
                <Button onClick={() => addIntervalToDay(day)}>Добавить время</Button>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  )
}

export default TimesPreview;