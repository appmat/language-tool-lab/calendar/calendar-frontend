import classes from './NewTemplateForm.module.css'
import {DatePicker, Button, Select, Input, Form, InputNumber} from 'antd';
import {useCallback, useState} from 'react'
import 'antd/dist/antd.css';
import TimesPreview from "./TimesPreview";
import {formatTemplate} from "../utils/TimeRecordUtils";
import axios from "axios";
import {baseUrl} from "../../constants";
import { successModal, errorModal} from "../utils/Modals";
import { useNavigate } from "react-router-dom";
import {useKeycloak} from "@react-keycloak/web";
const {Option} = Select;


const Template = {
  title: null,
  place: null,
  description: null,
  durationMin: '15',
  repeatEveryNthWeek: null,
  startDate: null,
  link: null
}

const validateMessages = {
  required: 'Поле обязательно'
}

function NewTemplateForm({template=Template}) {
  let navigate = useNavigate();
  const {TextArea} = Input;
  const [curTemplate, setTemplate] = useState(template)
  const [intervals, setIntervals] = useState()
  const { keycloak } = useKeycloak();

  const callback = useCallback((record) => {
    setIntervals(record)
  }, [])

  const submitHandler = async () => {
    const templateForm = {
      ...curTemplate,
      startDate: curTemplate['startDate'].format('YYYY-MM-DD'),
      durationMin: Number(curTemplate.durationMin),
      intervals: formatTemplate(intervals)
    }
    console.log(`${baseUrl}/meeting-template`)
    console.log('templateForm', templateForm.toString())
    let axiosConfig = {
      headers: {
        'Authorization': `Bearer ${keycloak.token}`
      }
    }
    try {
      await axios.post(`${baseUrl}/meeting-template`, templateForm, axiosConfig).then(() => {
        successModal({title: 'Шаблон успешно создан'})
      });
    } catch (error) {
      errorModal({error})
    }
    navigate(`/all-templates`)
  }

  const handleChange = (event) => {
    setTemplate((previousValue) => ({
      ...previousValue,
      [event.target.id]: event.target.value
    }))
  }

  const handleChangeForSelector = (event, name) => {
    setTemplate((previousValue) => ({
      ...previousValue,
      [name]: event
    }))
  }

  return (
    <div>
      <Form
        layout="vertical"
        onFinish={submitHandler}
        validateMessages={validateMessages}
        initialValues={template}
      >
        <div className={classes.form}>
          <div className={classes.cell}>
            <Form.Item
              className={classes.label}
              name='title'
              label='Название шаблона'
              onChange={handleChange}
              value={curTemplate.title}
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <Input/>
            </Form.Item>
          </div>
          <div className={classes.cell}>
            <Form.Item
              className={classes.label}
              name='description'
              label='Описание встречи'
              onChange={handleChange}
              value={curTemplate.description}
            >
              <TextArea rows={4} cols={30} placeholder="" maxLength={512}/>
            </Form.Item>
          </div>
          <div className={classes.cell}>
            <Form.Item
              className={classes.label}
              name='place'
              label='Место встречи'
              onChange={handleChange}
              value={curTemplate.place}
            >
              <TextArea rows={4} cols={30} placeholder="" maxLength={512}/>
            </Form.Item>
          </div>
          <div className={classes.cell}>
            <Form.Item
              className={classes.label}
              name='durationMin'
              label='Продолжительность встречи'
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <Select
                onChange={(event) => handleChangeForSelector(event, 'durationMin')}
              >
                <Option value="15">15 мин</Option>
                <Option value="30">30 минут</Option>
                <Option value="45">45 минут</Option>
                <Option value="60">1 час</Option>
                <Option value="90">1час 30минут</Option>
              </Select>
            </Form.Item>
          </div>
          <div className={classes.extracell}>
            <Form.Item
              className={classes.label}
              name='startDate'
              label='Дата начала'
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <DatePicker
                onChange={(event) => handleChangeForSelector(event, 'startDate')}
                picker="week"
                placeholder={''}/>
            </Form.Item>
          </div>
          <Form.Item
            label="Частота встречи (в неделях)"
            name='repeatEveryNthWeek'
            rules={[
              {
                required: true,
              },
            ]}
          >
            <InputNumber
              onChange={(event) => handleChangeForSelector(event, 'repeatEveryNthWeek')}
            />
          </Form.Item>
          <div className={classes.time}>
            <TimesPreview parenCallback={callback}/>
          </div>
          <div className={classes.cell}>
            <Form.Item
              className={classes.label}
              name='link'
              label='Добавьте ссылку для записи'
              onChange={handleChange}
              value={curTemplate.link}
            >
              <Input/>
            </Form.Item>
          </div>
          <Form.Item>
            <Button className={classes.btn} htmlType="submit">
              Сохранить
            </Button>
          </Form.Item>
        </div>
      </Form>
    </div>
  )
}

export default NewTemplateForm;