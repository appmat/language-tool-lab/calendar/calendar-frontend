import classes from './TemplateViewing.module.css'
import {Button, Descriptions, Modal} from 'antd';
import React from 'react'
import 'antd/dist/antd.css';
import * as _ from 'lodash';
import {baseUrl, dayMapping} from "../../constants";
import {getTimeIntervals} from '../utils/TimeRecordUtils'
import {DeleteOutlined, ExclamationCircleOutlined} from "@ant-design/icons";
import {Link} from "react-router-dom";
import axios from "axios";
import {errorModal, successModal} from "../utils/Modals";
import {useKeycloak} from "@react-keycloak/web";
import { useNavigate } from "react-router-dom";

const {confirm} = Modal;

const Template = {
  title: null,
  place: null,
  description: null,
  durationMin: '15',
  repeatEveryNthWeek: null,
  startDate: null,
  link: null
}

const convertTimeIntervals = (intervals) => {
  const groupedByDay = _.groupBy(intervals, 'weekday')
  const keys = _.keys(groupedByDay)
  return (
    _.map(keys, (day, index) => (
      <div key={day}>
        {dayMapping[day]}
        <div className={classes.time} key={index}>
          {getTimeIntervals(_.map(groupedByDay[day], 'timeInterval'))}
        </div>
      </div>
    ))
  )
}

function TemplateViewing({template=Template}) {
  const { keycloak } = useKeycloak();
  let navigate = useNavigate();
  const handleOk = async () => {
    try {
      await axios.delete(`${baseUrl}/meeting-template/${template.id}`,
        {
          headers: {
            'Authorization': `Bearer ${keycloak.token}`
          }
        }).then(() => {
        successModal({title:'Шаблон удалён'})

      });
    } catch (error) {
      errorModal({error, title: 'Произошла ошибка при удалении шаблона'})
    }
    navigate('/all-templates')
  };

  const handleCancel = () => {
    console.log('cancel')
  };

  const DeleteModalForm = () => {
    confirm({
      title: 'Вы уверены, что хотите удалить данный шаблон?',
      icon: <ExclamationCircleOutlined />,
      okText: 'Да',
      okType: 'danger',
      cancelText: 'Нет',

      onOk() {
        handleOk()
      },
      onCancel() {
        handleCancel()
      },
    });
  };

  return (
    <div className={classes.content}>
      <Descriptions
        bordered
        size="small"
        title="Информация о шаблоне"
        extra={
        <div>
          <div className={classes.change}>
            <Link to={'/template-edit/' + template.id} state={template}>
              <Button type="primary">Изменить</Button>
            </Link>
          </div>
          <Button icon={<DeleteOutlined onClick={DeleteModalForm}/>}/>
        </div>
        }
      >
        <Descriptions.Item label="Название шаблона" span={3}>{template.title}</Descriptions.Item>
        <Descriptions.Item label="Описание встречи" span={3}>{template.description}</Descriptions.Item>
        <Descriptions.Item label="Место встречи" span={3}>{template.place}</Descriptions.Item>
        <Descriptions.Item label="Продолжительность встречи" span={3}>{template.durationMin}</Descriptions.Item>
        <Descriptions.Item label="Дата начала" span={3}>{template.startDate}</Descriptions.Item>
        <Descriptions.Item label="Частота встречи (в неделях)" span={3}>{template.repeatEveryNthWeek}</Descriptions.Item>
        <Descriptions.Item label="Ссылка для записи" span={3}>{template.link}</Descriptions.Item>
        <Descriptions.Item label="Интервалы" span={3}>{convertTimeIntervals(template.intervals)}</Descriptions.Item>
      </Descriptions>
    </div>
  )
}

export default TemplateViewing;