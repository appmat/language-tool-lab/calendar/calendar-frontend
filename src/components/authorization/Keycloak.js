import Keycloak from "keycloak-js";

const keycloak = new Keycloak({
  "url": "https://auth.appmat.org/auth/",
  "realm": "mpei",
  "clientId": "calendar",
  "ssl-required": "external",
  "resource": "calendar",
  "public-client": true,
  "confidential-port": 0
})
console.log('keycloak', keycloak)

export default keycloak;