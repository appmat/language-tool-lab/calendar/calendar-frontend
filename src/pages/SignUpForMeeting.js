import CalendarView from '../components/calendar/CalendarView'
import {useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import axios from "axios";
import {baseUrl} from "../constants";
import {useKeycloak} from "@react-keycloak/web";

function SignUpForMeeting(){
  let params = useParams()
  const [template, setTemplate] = useState({})
  const { keycloak } = useKeycloak();

  useEffect(() => {
    axios.get(`${baseUrl}/meeting-template/${params.templateId}`,{
      headers: {
        'Authorization': `Bearer ${keycloak.token}`
      }
    })
      .then(response => setTemplate(response.data))
  }, [params.templateId, keycloak])

  return (
    <div>
      <h2>Запись на {`"${template.title}"`}</h2>
      <p>{`Длительность встречи: ${template.durationMin} минут`}</p>
      <p>{`Место проведение: ${template.place}`}</p>
      <CalendarView template={template}/>
    </div>
  )
}

export default SignUpForMeeting;