import NewTemplateForm from "../components/newTemplate/NewTemplateForm";

function NewTemplate(){
  return (
    <div>
      <h1>Создание встречи один-на-один</h1>
      <NewTemplateForm/>
    </div>
  )
}

export default NewTemplate;