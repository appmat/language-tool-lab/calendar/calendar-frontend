import {Divider, Steps} from 'antd';
import {Typography} from 'antd';
import { UserOutlined } from '@ant-design/icons';
const {Step} = Steps;
const { Title } = Typography;

function HomePage() {
  return (
    <div>
      <Title level={2}>Календарь встреч</Title>
      <Title level={5}><UserOutlined /> Авториазация по учётной записи МЭИ</Title>
      <Divider/>
      <Title level={5}>Что может сервис?</Title>
      <Steps  current={2} progressDot direction="vertical">
        <Step title="Создавайте шаблоны" description="Шаблоны встреч можно гибко настраивать под себя, выбирая удобные интервалы."/>
        <Step title="Делитись" description="Поделитесь ссылкой на созданный дашборд для записи"/>
        <Step title="Записывайтесь" description="Записывайтесь на встречи по шаблону других пользователей"/>
      </Steps>
    </div>
  )
}

export default HomePage;