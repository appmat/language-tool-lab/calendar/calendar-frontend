import { useLocation } from "react-router-dom";
import moment from "moment";
import NewTemplateForm from "../components/newTemplate/NewTemplateForm";
import React from "react";
import classes from '../components/newTemplate/EditTemplatePage.css'


function EditTemplatePage() {
  let template = useLocation().state;
  const formattedTemplate = {...template, startDate: moment(template.startDate)}

  return (
    <div className={classes.content}>
      <h2> Редактирование шаблона</h2>
      <NewTemplateForm template={formattedTemplate}/>
    </div>
  )
}

export default EditTemplatePage;