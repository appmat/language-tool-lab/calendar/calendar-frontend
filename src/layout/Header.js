import classes from './Header.module.css'
import {Button} from 'antd'
import {useKeycloak} from "@react-keycloak/web";


function Header() {
  const { keycloak } = useKeycloak();

  return (
    <header className={classes.header}>
      <div className={classes.logo}>Календарь встреч</div>
      <div className={classes.auth}>
        {!keycloak.authenticated && (
          <Button
            onClick={() => keycloak.login()}
          >
            Войти
          </Button>
        )}
        {!!keycloak.authenticated && (
          <Button
            onClick={() => keycloak.logout()}
          >
            Выйти
          </Button>
        )}
      </div>
    </header>
  )
}

export default Header;