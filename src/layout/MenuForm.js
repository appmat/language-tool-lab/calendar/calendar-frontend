import {Menu} from 'antd';
import 'antd/dist/antd.css';
import {CalendarOutlined, ToolOutlined, SnippetsOutlined} from '@ant-design/icons';
import {Link} from "react-router-dom";

function MenuForm() {

  return (
    <Menu style={{width: 256}} mode="vertical">
      <Menu.Item key="meetings" icon={<CalendarOutlined/>}>
        <Link to="/meetings">
          Ближайшие встречи
        </Link>
      </Menu.Item>
      <Menu.Item key="templates" icon={<SnippetsOutlined/>}>
        <Link to="/all-templates">
          Мои шаблоны
        </Link>
      </Menu.Item>
      <Menu.Item key="settings" icon={<ToolOutlined/>}>
        <Link to="/">
          Настройки
        </Link>
      </Menu.Item>
    </Menu>
  )
}

export default MenuForm;