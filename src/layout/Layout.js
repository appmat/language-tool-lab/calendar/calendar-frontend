import Header from './Header'
import classes from './Layout.module.css'
import MenuForm from './MenuForm'

function Layout(props) {
  return (
    <div>
      <Header/>
      <div className={classes.main}>
        <div className={classes.menu}>
          <MenuForm/>
        </div>
        <main className={classes.content}>{props.children}</main>
      </div>
      <div className={classes.footer}>
      </div>
    </div>
  );
}

export default Layout;