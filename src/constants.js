export const DAYS_OF_WEEK = [ 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY' ]
export const DEFAULT_DAY_OF_WEEK = [ 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY']

export const dayMapping = {
  'MONDAY': 'ПН',
  'TUESDAY': 'ВТ',
  'WEDNESDAY': 'СР',
  'THURSDAY': 'ЧТ',
  'FRIDAY': 'ПТ',
  'SATURDAY': 'СБ',
  'SUNDAY': 'ВС'
}

export const dayMappingToEng = {
  'ПН': 'MONDAY',
  'ВТ': 'TUESDAY',
  'СР': 'WEDNESDAY',
  'ЧТ': 'THURSDAY',
  'ПТ': 'FRIDAY',
  'СБ': 'SATURDAY',
  'ВС': 'SUNDAY'
}

export const baseUrl = 'https://appmat-language-tool-lab-calendar-calendar-core.appmat.org/api'
export const serviceUrl = 'https://calendar.appmat.org'
// export const serviceUrl = 'http://localhost:3000'